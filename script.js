const steps = document.querySelectorAll(".stp");
const circleSteps = document.querySelectorAll(".step");
const formInputs = document.querySelectorAll(".card-item input");
const plans = document.querySelectorAll(".plans-card");
const toggleSwitch = document.querySelector(".selection");
const addons = document.querySelectorAll(".add-ons-card");
const changeLink = document.querySelector(".plan-info a");
let formSubmitted = false;
let currentStep = 0;
let currentCircle = 0;
const obj = {
  plan: null,
  price: null,
  kind: null,
};
let formData = {};

//function to store input data
function storeFormData() {
  formData = {
    name: document.getElementById("name").value,
    email: document.getElementById("email").value,
    phone: document.getElementById("phone").value,
  };
  localStorage.setItem("formData", JSON.stringify(formData));
}

//function to populate data
function populateFormData() {
  var storedData = localStorage.getItem("formData");
  if (storedData) {
    var formData = JSON.parse(storedData);
    document.getElementById("name").value = formData.name;
    document.getElementById("email").value = formData.email;
    document.getElementById("phone").value = formData.phone;
  }
}

//function to hide plan error
function hidePlanErrorMessage() {
  const errorMessage = document.getElementById("plan-error");
  errorMessage.textContent = "";
  errorMessage.style.display = "none";
}

//Intially hiding all steps and showing first step only
steps.forEach((step, index) => {
  if (index !== currentStep) {
    step.style.display = "none";
  }
});

//Iterating through each step
steps.forEach((step) => {
  const nextPage = step.querySelector("#button");
  const previousPage = step.querySelector("#previous-page");
  if (previousPage) {
    previousPage.addEventListener("click", () => {
      steps[currentStep].style.display = "none";
      circleSteps[currentStep].classList.remove("active");
      currentStep--;
      populateFormData();
      steps[currentStep].style.display = "flex";
      currentCircle = currentStep;
      circleSteps[currentCircle].classList.add("active");
    });
  }
  if (nextPage) {
    nextPage.addEventListener("click", () => {
      formSubmitted = true;
      if (currentStep === 1 && !obj.plan) {
        const errorMessage = document.getElementById("plan-error");
        errorMessage.textContent = "Please select a plan";
        errorMessage.style.display = "block";
        return;
      }
      if (validateForm() && currentStep < steps.length - 1) {
        storeFormData();
        steps[currentStep].style.display = "none";
        currentStep++;
        steps[currentStep].style.display = "flex";
        circleSteps[currentCircle].classList.add("active");
      }
      circleSteps.forEach((circle, circleIndex) => {
        if (circleIndex === currentStep) {
          circle.classList.add("active");
        } else {
          circle.classList.remove("active");
        }
      });
      populateSelectedPlans();
    });
  }
});

//Move to step2 if we click on change
changeLink.addEventListener("click", (event) => {
  event.preventDefault();
  steps[currentStep].style.display = "none";
  circleSteps[currentStep].classList.remove("active");
  currentStep = 1;
  steps[currentStep].style.display = "flex";
  currentCircle = currentStep;
  circleSteps[currentCircle].classList.add("active");
});

// Add event listeners to input fields
formInputs.forEach((input) => {
  input.addEventListener("change", () => {
    validateInput(input);
  });
});

// Function to validate individual input field
function validateInput(input) {
  const value = input.value.trim();
  removeError(input);
  if (!value) {
    addError(input, "This field is required");
  } else if (input.id === "email") {
    if (!(value.includes("@") && value.includes("."))) {
      addError(input, "Enter a valid email");
    } else {
      removeError(input);
    }
  } else if (input.id === "phone") {
    const phoneRegex = /^\d{10}$/;
    if (!phoneRegex.test(value)) {
      addError(input, "Enter a valid number");
    } else {
      removeError(input);
    }
  }
}

// Update the validateForm() function to validate all input fields
function validateForm() {
  let valid = true;

  if (formSubmitted) {
    for (let i = 0; i < formInputs.length; i++) {
      const input = formInputs[i];
      validateInput(input);

      if (input.classList.contains("err")) {
        valid = false;
      }
    }
  }

  return valid;
}

//function to add error message
function addError(input, errorMessage) {
  input.classList.add("err");
  const label = findLabel(input).nextElementSibling;
  label.style.display = "flex";
  label.textContent = errorMessage;
}

// function to remove error message
function removeError(input) {
  input.classList.remove("err");
  const label = findLabel(input).nextElementSibling;
  label.style.display = "none";
  label.textContent = "";
}

//function to find label
function findLabel(label) {
  const id = label.id;
  const labels = document.getElementsByTagName("label");
  for (let i = 0; i < labels.length; i++) {
    if (labels[i].htmlFor === id) return labels[i];
  }
}

//iterating through each plan
plans.forEach((plan) => {
  plan.addEventListener("click", () => {
    const selectedPlan = document.querySelector(".selected");
    if (selectedPlan) {
      selectedPlan.classList.remove("selected");
    }
    plan.classList.add("selected");
    const planName = plan.querySelector("h3").textContent;
    const planPrice = plan.querySelector("p").textContent;
    obj.plan = planName;
    obj.price = planPrice;
    hidePlanErrorMessage();
  });
});

//toggleswitch functionality
toggleSwitch.addEventListener("click", () => {
  const val = toggleSwitch.querySelector("input").checked;
  if (val) {
    document.querySelector(".monthly").classList.remove("switch-active");
    document.querySelector(".yearly").classList.add("switch-active");
    obj.plan = null;
    obj.price = null;
    const selectedPlan = document.querySelector(".selected");
    if (selectedPlan) {
      selectedPlan.classList.remove("selected");
    }
  } else {
    document.querySelector(".monthly").classList.add("switch-active");
    document.querySelector(".yearly").classList.remove("switch-active");
  }
  switchPrice(val);
  obj.kind = val;
});

//function to change plan prices
function switchPrice(checked) {
  const yearlyPrice = [90, 120, 150];
  const monthlyPrice = [9, 12, 15];
  const addonMonthlyPrice = [1, 2, 2];
  const addonYearlyPrice = [10, 20, 20];
  const prices = document.querySelectorAll(".plans-card p");
  const addonPrices = document.querySelectorAll("#cost");
  if (checked) {
    prices[0].innerHTML = `+$${yearlyPrice[0]}/yr<br><span class="free-text">2 months free</span>`;
    prices[1].innerHTML = `+$${yearlyPrice[1]}/yr<br><span class="free-text">2 months free</span>`;
    prices[2].innerHTML = `+$${yearlyPrice[2]}/yr<br><span class="free-text">2 months free</span>`;
    addonPrices[0].innerHTML = `+$${addonYearlyPrice[0]}/yr`;
    addonPrices[1].innerHTML = `+$${addonYearlyPrice[1]}/yr`;
    addonPrices[2].innerHTML = `+$${addonYearlyPrice[2]}/yr`;
  } else {
    prices[0].innerHTML = `+$${monthlyPrice[0]}/mo`;
    prices[1].innerHTML = `+$${monthlyPrice[1]}/mo`;
    prices[2].innerHTML = `+$${monthlyPrice[2]}/mo`;
    addonPrices[0].innerHTML = `+$${addonMonthlyPrice[0]}/mo`;
    addonPrices[1].innerHTML = `+$${addonMonthlyPrice[1]}/mo`;
    addonPrices[2].innerHTML = `+$${addonMonthlyPrice[2]}/mo`;
  }
}

//iterating through each addon
addons.forEach((addon) => {
  const addonSelect = addon.querySelector("input[type='checkbox']");
  addonSelect.addEventListener("click", (e) => {
    e.stopPropagation();
    addon.classList.toggle("ad-selected");
  });
  addon.addEventListener("click", () => {
    const addonSelect = addon.querySelector("input[type='checkbox']");
    addonSelect.checked = !addonSelect.checked;
    if (addonSelect.checked) {
      addon.classList.add("ad-selected");
    } else {
      addon.classList.remove("ad-selected");
    }
  });
});

//function to populate selected plans and addons
function populateSelectedPlans() {
  const planNameElement = document.querySelector(".plan-name");
  const planPriceElement = document.querySelector(".plan-price");
  const selectedAddonsLine = document.getElementById("selected-addons-line");
  const totalPriceElement = document.getElementById("total");
  selectedAddonsLine.innerHTML = "";
  if (obj.plan && obj.price) {
    const val = toggleSwitch.querySelector("input").checked;
    const billingPeriod = val ? "(Yearly)" : "(Monthly)";
    planNameElement.textContent = `${obj.plan} ${billingPeriod}`;
    const planPrice = val ? obj.price.split("/")[0] : obj.price;
    planPriceElement.textContent = `${planPrice}${val ? "/yr" : ""}`;
    const selectedAddons = Array.from(addons)
      .filter((addon) => addon.querySelector("input[type='checkbox']").checked)
      .map((addon) => {
        const addonName = addon.querySelector("h4").textContent;
        const addonCost = addon.querySelector("#cost").textContent;
        return { name: addonName, cost: addonCost };
      });
    selectedAddons.forEach((addon) => {
      const addonElement = document.createElement("div");
      addonElement.classList.add("selected-addon");
      const addonNameElement = document.createElement("h4");
      const addonCostElement = document.createElement("h4");
      addonNameElement.textContent = addon.name;
      addonCostElement.textContent = addon.cost;
      addonNameElement.style.display = "inline";
      addonCostElement.style.display = "inline";
      addonNameElement.classList.add("margin");
      addonElement.appendChild(addonNameElement);
      addonElement.appendChild(addonCostElement);
      selectedAddonsLine.appendChild(addonElement);
    });
    const selectedPlanPrice = parseFloat(planPrice.replace(/[^0-9.-]+/g, ""));
    const addonPrices = selectedAddons.map((addon) => {
      const addonCost = addon.cost.replace("$", "");
      return parseFloat(addonCost);
    });
    const totalAddonsPrice = addonPrices.reduce((sum, price) => sum + price, 0);
    const totalPrice = selectedPlanPrice + totalAddonsPrice;
    totalPriceElement.textContent = `$${totalPrice.toFixed(0)}${
      val ? "/yr" : "/mo"
    }`;
  }
}
